#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json
import random
import tweepy
import time

CONSUMER_KEY = os.environ.get('CONSUMER_KEY')
CONSUMER_SECRET = os.environ.get('CONSUMER_SECRET')
ACCESS_KEY = os.environ.get('ACCESS_KEY')
ACCESS_SECRET = os.environ.get('ACCESS_SECRET')
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

with open('prefixes.json', encoding='utf-8', mode='r') as prefixes_file:
    prefixes = json.load(prefixes_file)
with open('suffixes.json', encoding='utf-8', mode='r') as suffixes_file:
    suffixes = json.load(suffixes_file)
with open('gundams.json', encoding='utf-8', mode='r') as gundams_file:
    gundams = json.load(gundams_file)

random.seed()
WAIT_TIME_SECONDS = os.environ.get('WAIT_TIME_SECONDS')

while True:
    rand_prefix = random.randint(0, len(prefixes['list']) - 1)
    prefix = prefixes['list'][rand_prefix]
    rand_suffix = random.randint(0, len(suffixes['list']) - 1)
    suffix = suffixes['list'][rand_suffix]
    rand_gundam = random.randint(0, len(gundams['list']) - 1)
    gundam = gundams['list'][rand_gundam]

    rand = random.randint(1, 5)
    if rand == 5:
        custom_gundam_name = prefix + ' ' + gundam + ' ' + suffix
        #print(custom_gundam_name)
        api.update_status(custom_gundam_name)
    elif rand == 3 or rand == 4:
        custom_gundam_name = prefix + ' ' + gundam
        #print(custom_gundam_name)
        api.update_status(custom_gundam_name)
    elif rand == 1 or rand == 2:
        custom_gundam_name = gundam + ' ' + suffix
        #print(custom_gundam_name)
        api.update_status(custom_gundam_name)
    time.sleep(WAIT_TIME_SECONDS)
